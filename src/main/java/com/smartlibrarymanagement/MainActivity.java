package com.smartlibrarymanagement;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.smartlibrarymanagement.domain.Book;
import com.smartlibrarymanagement.repository.BookDbHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    BookDbHelper bookDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bookDbHelper = new BookDbHelper(getApplicationContext());
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent bookActivity = new Intent(getBaseContext(), BookActivity.class);
                startActivity(bookActivity);
            }
        });


        ListView bookList = findViewById(R.id.bookList);
        final Cursor cursor = bookDbHelper.fetchAllBooks();
        SimpleCursorAdapter bookListAdapter = new SimpleCursorAdapter(getApplicationContext(),
                android.R.layout.simple_list_item_1,
                cursor,
                new String[]{BookDbHelper.COLUMN_BOOK_TITLE},
                new int[]{android.R.id.text1}, 0);
        bookList.setAdapter(bookListAdapter);

        bookList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursorBook = (Cursor) parent.getItemAtPosition(position);
                Book book = BookDbHelper.cursorToBook(cursorBook);
                Intent bookActivity = new Intent(getBaseContext(), BookActivity.class);
                bookActivity.putExtra("BOOK", book);
                startActivity(bookActivity);
                finish();
            }
        });
        bookList.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, final ContextMenu.ContextMenuInfo menuInfo) {
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.book_menu, menu);
                MenuItem menuItem = menu.getItem(0);
                final long bookId = ((AdapterView.AdapterContextMenuInfo) menuInfo).id;
                Log.i("Test2", "TEST2");

                menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener(){
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Log.i("Test", "TEST");
                        bookDbHelper.deleteBook(cursor);
                        Toast.makeText(MainActivity.this, "The book "+cursor.getString((int) bookId)+" is deleted .",
                                Toast.LENGTH_LONG).show();
                        return false;
                    }

                });

            }

        });
    }

    public List<Book> getAllBooks() {
        ArrayList<Book> books = new ArrayList<Book>();
        Cursor bookCursor = bookDbHelper.fetchAllBooks();
        if (bookCursor.moveToFirst()) {
            while (!bookCursor.isAfterLast()) {
                Book book = bookDbHelper.cursorToBook(bookCursor);
                books.add(book);
                bookCursor.moveToNext();
            }
        }
        bookCursor.close();
        return books;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

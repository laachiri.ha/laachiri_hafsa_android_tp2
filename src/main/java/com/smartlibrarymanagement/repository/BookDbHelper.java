package com.smartlibrarymanagement.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.smartlibrarymanagement.domain.Book;

public class BookDbHelper extends SQLiteOpenHelper {

    private static final String TAG = BookDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "book.db";

    public static final String TABLE_NAME = "library";

    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";

    public BookDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        // db.execSQL() with the CREATE TABLE ... command
        db.execSQL("CREATE TABLE IF NOT EXISTS "+TABLE_NAME+" (" +
                _ID +" INTEGER PRIMARY KEY AUTOINCREMENT,"+
                COLUMN_BOOK_TITLE +" TEXT UNIQUE,"+
                COLUMN_AUTHORS +" TEXT ,"+
                COLUMN_YEAR +" TEXT ,"+
                COLUMN_GENRES +" TEXT ,"+
                COLUMN_PUBLISHER +" TEXT"+
        ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    /**
     * Adds a new book
     * @return  true if the book was added to the table ; false otherwise
     */
    public boolean addBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();



        // Inserting Row
        long rowID = 0;
        // call db.insert()
        ContentValues values = new ContentValues();
        values.put(COLUMN_BOOK_TITLE, book.getTitle());
        values.put(COLUMN_AUTHORS, book.getAuthors());
        values.put(COLUMN_YEAR, book.getYear());
        values.put(COLUMN_GENRES, book.getGenres());
        values.put(COLUMN_PUBLISHER, book.getPublisher());

        rowID = db.insert(TABLE_NAME, null, values);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a book inside the data base
     * @return the number of updated rows
     */
    public int updateBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        int res = 0;

        // updating row
        // call db.update()

        ContentValues values = new ContentValues();
        values.put(COLUMN_BOOK_TITLE, book.getTitle());
        values.put(COLUMN_AUTHORS, book.getAuthors());
        values.put(COLUMN_YEAR, book.getYear());
        values.put(COLUMN_GENRES, book.getGenres());
        values.put(COLUMN_PUBLISHER, book.getPublisher());

        res = db.update(TABLE_NAME,values,_ID+"="+book.getId(),null);
        return res;
    }


    public Cursor fetchAllBooks() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = null;
        // call db.query()

        cursor = db.query(TABLE_NAME,new String[]{_ID,COLUMN_BOOK_TITLE,COLUMN_AUTHORS,COLUMN_YEAR,COLUMN_GENRES,COLUMN_PUBLISHER},null,null,null,null,null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public void deleteBook(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        // call db.delete();
        int idIndex = cursor.getColumnIndex(_ID);
        String id = cursor.getString(idIndex);
        db.delete(TABLE_NAME,_ID+"="+id, null);
        db.close();
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addBook(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        addBook(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        addBook(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        addBook(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        addBook(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        addBook(new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        addBook(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        addBook(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        addBook(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        addBook(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        addBook(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

    public static Book cursorToBook(Cursor cursor) {
        Book book = null;
        if(cursor != null){
            book = new Book();
            int idIndex = cursor.getColumnIndex(_ID);
            int titleIndex = cursor.getColumnIndex(COLUMN_BOOK_TITLE);
            int authorIndex = cursor.getColumnIndex(COLUMN_AUTHORS);
            int genreIndex = cursor.getColumnIndex(COLUMN_GENRES);
            int publisherIndex = cursor.getColumnIndex(COLUMN_PUBLISHER);
            int yearIndex = cursor.getColumnIndex(COLUMN_YEAR);
            book.setTitle(cursor.getString(titleIndex));
            book.setAuthors(cursor.getString(authorIndex));
            book.setGenres(cursor.getString(genreIndex));
            book.setPublisher(cursor.getString(publisherIndex));
            book.setYear(cursor.getString(yearIndex));
            book.setId(Long.parseLong(cursor.getString(idIndex)));
        }
        // build a Book object from cursor
        return book;
    }
}
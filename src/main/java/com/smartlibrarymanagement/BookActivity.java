package com.smartlibrarymanagement;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.smartlibrarymanagement.domain.Book;
import com.smartlibrarymanagement.repository.BookDbHelper;

import static android.app.ProgressDialog.show;

public class BookActivity extends AppCompatActivity {

    BookDbHelper bookDbHelper;
    public void showMessage(String title , String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        bookDbHelper = new BookDbHelper(getApplicationContext());
        Button sauvegarderBtn = findViewById(R.id.button);
        final EditText titleEditText = findViewById(R.id.nameBook);
        final EditText authorEditText = findViewById(R.id.editAuthors);
        final EditText yearEditText = findViewById(R.id.editYear);
        final EditText genreEditText = findViewById(R.id.editGenres);
        final EditText publisherEditText = findViewById(R.id.editPublisher);

        final Book book = (Book) getIntent().getParcelableExtra("BOOK");
        Long bookId = null;
        if(book != null){
            titleEditText.setText(book.getTitle());
            authorEditText.setText(book.getAuthors());
            yearEditText.setText(book.getYear());
            genreEditText.setText(book.getGenres());
            publisherEditText.setText(book.getPublisher());
        }

        sauvegarderBtn.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  Book newBook = new Book();
                  newBook.setTitle(titleEditText.getText().toString());
                  newBook.setAuthors(authorEditText.getText().toString());
                  newBook.setYear(yearEditText.getText().toString());
                  newBook.setGenres(genreEditText.getText().toString());
                  newBook.setPublisher(publisherEditText.getText().toString());
                  if(book != null){ // book existe so it an update
                      newBook.setId(book.getId());
                      int updated = bookDbHelper.updateBook(newBook);
                      if(updated > 0){
                          Log.i("Book", "Book "+newBook.getTitle()+" is updated.");
                      }
                      else {
                          Log.i("Book", "No books are updated.");
                      }
                  }
                  else{// book dose not existe (new book)
                      boolean isSaved = bookDbHelper.addBook(newBook);
                      if(isSaved){
                          showMessage("Message","Book "+newBook.getTitle()+"is saved.");
                          Log.i("Book", "Book "+newBook.getTitle()+" is saved.");
                      }
                      else{
                          showMessage("Message",newBook.getTitle()+" already exist .");
                          Log.e("Book", "Can not save Book "+newBook.getTitle()+".");

                      }
                  }
                  Intent mainActivity = new Intent(getBaseContext(),MainActivity.class);
                  startActivity(mainActivity);
              }
          }

        );
    }
}
